class TweetsController < ApplicationController
        protect_from_forgery except: :search # searchアクションを除外
    def index
        @tweets = Tweet.all
    end
    def new
     @tweet = Tweet.new
    end
    def create
     message= params[:tweet][:message]
     tdate = params[:tweet][:tdate]
     @tweet = Tweet.new(message: message, tdate: tdate)
     if @tweet.save
       flash[:notice] = "1 record added"
       redirect_to root_path
     else
       render 'new'
     end
    end
    def destroy
     tweet = Tweet.find(params[:id])
     tweet.destroy
     redirect_to root_path
    end
    def edit
     @tweet = Tweet.find(params[:id])
    end
    def update
     message= params[:tweet][:message]
     tdate = params[:tweet][:tdate]
     @tweet = Tweet.find(params[:id])
     @tweet.update(message: message, tdate: tdate)
     if @tweet.save
       flash[:notice] = "1 record update"
       redirect_to root_path
     else
       render 'edit'
     end
    end
    def show
     @tweet = Tweet.find(params[:id])
    end
end
